#ifndef __ONEWIRE_H__
#define __ONEWIRE_H__
#include <avr/io.h>
#include <stdbool.h>

/* Pin definitions */

#define ONEWIRE_PORT_LETTER A
#define ONEWIRE_PIN  1

/* --------------- */

void onewire_init(void);
bool onewire_reset_probe(void);
void onewire_write(unsigned char);
unsigned char onewire_read(void);

/* General 1wire commands */
#define ONEWIRE_READ_ROM 0x33
#define ONEWIRE_MATCH_ROM 0x55
#define ONEWIRE_SKIP_ROM 0xCC
#define ONEWIRE_ALARM_SEARCH 0xEC
#define ONEWIRE_SEARCH_ROM 0xF0
/* DS18B20 specific commands */
#define DS18B20_CONVERT_T 0x44
#define DS18B20_READ_SCRATCHPAD 0xBE

#endif /* __ONEWIRE_H__ */

#include <util/delay.h>
#include <util/atomic.h>
#include "onewire.h"

#ifndef __CONCAT
#define __CONCATENATE(left,right) left ## right
#define __CONCAT(left,right) CONCATENATE(left,right)
#endif

#define ONEWIRE_PORT __CONCAT(PORT,ONEWIRE_PORT_LETTER)
#define ONEWIRE_DIRP __CONCAT(DDR,ONEWIRE_PORT_LETTER)
#define ONEWIRE_INPORT __CONCAT(PIN,ONEWIRE_PORT_LETTER)

/* Clear PORTn, all logic is performed with direction port
*/
void onewire_init(void)
{
    ONEWIRE_PORT &= ~(1 << ONEWIRE_PIN);
    ONEWIRE_DIRP &= ~(1 << ONEWIRE_PIN);
}

inline static void onewire_pull_down(void)
{
   ONEWIRE_DIRP |= (1 << ONEWIRE_PIN);
}

/* Release the pin, let external pull-up set voltage HI */
inline static void onewire_release(void)
{
   ONEWIRE_DIRP &= ~(1 << ONEWIRE_PIN);
}

inline static char onewire_line_state(void)
{
   return (ONEWIRE_INPORT & (1 << ONEWIRE_PIN));
}

/* Send reset signal and read if anything answers */
bool onewire_reset_probe(void)
{
   // External pull up OK ?
   bool ret = (onewire_line_state() != 0);
   ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
           onewire_pull_down();
           _delay_us(480);
           onewire_release();
           _delay_us(70);
           ret &= (onewire_line_state() == 0);
           _delay_us(410);
   }
   return ret;
}


/* Write one bit to the bus
 *
 * for 1 pull LOW for [ 1; 15 ] us
 * for 0 pull LOW for [ 60; 120 ] us
*/
static void onewire_write_bit(char bit)
{
    if (bit) {
            ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                    onewire_pull_down();
                    _delay_us(6);
                    onewire_release();
                    _delay_us(64);
            }
    }
    else {
            ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                    onewire_pull_down();
                    _delay_us(60);
                    onewire_release();
                    _delay_us(10);
            }
    }
}

/* Read one bit from the bus
 * Sample the line 15 us after pulling it low for at least 1 us
*/
static bool onewire_read_bit(void)
{
    bool ret;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
           onewire_pull_down();
           _delay_us(6);
           onewire_release();
           _delay_us(9);
           ret = (onewire_line_state() == 0) ? false : true;
           _delay_us(55); // Let the rest of the time slot expire.
    }
    return ret;
}

/* Write one byte to the bus
 * Order is LSB.
*/
void onewire_write(unsigned char byte)
{
    unsigned char bitno;
    for (bitno = 0; bitno < 8; ++bitno)
        onewire_write_bit(byte & (1 << bitno));
}

/* Read one byte from the bus
 * Order is LSB.
*/
unsigned char onewire_read(void)
{
    unsigned char res = 0;
    unsigned char bitno;
    for (bitno = 0; bitno < 8; ++bitno)
         if (onewire_read_bit())
            res |= (1 << bitno);
    return res;
}

#ifndef __RINGBUF_H__
#define __RINGBUF_H__
#include <stdint.h>

#if (RINGBUF_SIZE < CHAR_MAX)
    typedef char rb_index_t;
#else
    typedef intptr_t rb_index_t;
#endif

#ifndef RINGBUF_ITEM_T 
    typedef char rb_item_t;
#else
    typedef RINGBUF_ITEM_T rb_item_t;
#endif

typedef struct {
    rb_index_t p;
    rb_item_t buf[RINGBUF_SIZE];
} RINGBUF;

static void rb_reset(RINGBUF* rb)
{
    rb->p = 0;
}

static void rb_fill(RINGBUF* rb, rb_item_t val)
{
    rb_index_t i;
    for (i=0; i < RINGBUF_SIZE; ++i) 
       rb->buf[i] = val;
}

static void rb_push_front(RINGBUF* rb, const rb_item_t item)
{
    rb->buf[rb->p++] = item;
    if (rb->p == RINGBUF_SIZE)
        rb->p = 0;
}

static rb_item_t rb_get(const RINGBUF* rb, const unsigned int offset)
{
    return rb->buf[(rb->p + offset) % RINGBUF_SIZE];
}

#endif /* __RINGBUF_H__ */

#include "lcd_c55.h"


static void lcd_setup_ports(void)
{
    LCD_RST_PORT |= (1 << LCD_RST_PIN);
    LCD_DC_PORT &= ~(1 << LCD_DC_PIN);
    LCD_CS_PORT |= (1 << LCD_CS_PIN);

    LCD_RST_DIRP |= (1 << LCD_RST_PIN);
    LCD_DC_DIRP |= (1 << LCD_DC_PIN);
    LCD_CS_DIRP |= (1 << LCD_CS_PIN);

    spi_init();
    _delay_ms(50);
    LCD_CS_PORT &= (1 << LCD_CS_PIN);
    _delay_ms(1);
    LCD_RST_PORT &= (1 << LCD_RST_PIN);
    _delay_ms(1);
    LCD_RST_PORT |= (1 << LCD_RST_PIN);
    _delay_ms(1);
    LCD_CS_PORT |= (1 << LCD_CS_PIN);

}

void lcd_init(void)
{
    lcd_setup_ports();
    lcd_send(COMMAND, 0x20); // Power up standard command set
    lcd_send(COMMAND, 0x10); // internal HV-gen disabled
    lcd_send(COMMAND, 0x21); // ext. command set
    lcd_send(COMMAND, 0x05); // temperature coeff. [0x04 ; 0x07]
    //lcd_send(COMMAND, 0x08); // internal HV-gen x2
    lcd_send(COMMAND, 0x80); // Vop set to minimum
    lcd_send(COMMAND, 0x16); // bias
    lcd_send(COMMAND, 0x20); // standard command set
    lcd_send(COMMAND, 0x0C); // nomal mode non-inverted
}


void lcd_send(DC_T type, uint8_t byte)
{
    LCD_CS_PORT &= ~(1 << LCD_CS_PIN); // cs low active
    if (type == DATA)
        LCD_DC_PORT |= (1 << LCD_DC_PIN);
    else
        LCD_DC_PORT &= ~(1 << LCD_DC_PIN);
    {
        _delay_us(SPI_DELAY_US);
        spi_send(byte);
        _delay_us(SPI_DELAY_US);
    }
    _delay_us(SPI_DELAY_US);
    LCD_CS_PORT |= (1 << LCD_CS_PIN); // cs hi inactive
}

void lcd_move(int x, int y)
{
    lcd_send(COMMAND, 0x80 + (x & 0x7F)); // in normal command set
    lcd_send(COMMAND, 0x40 + (y & 0x3F));
}

void lcd_clear(void)
{
    lcd_move(0, 0);
    unsigned int octet_no = (LCD_BANKS*LCD_PIXEL_COLUMNS);
    while (octet_no-->0)
        lcd_send(DATA, 0);
    lcd_move(0, 0);
}
    

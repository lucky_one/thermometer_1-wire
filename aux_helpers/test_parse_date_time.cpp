#define CATCH_CONFIG_MAIN
#include "catch.h"




TEST_CASE( "Factorial of 0 is 1 (fail)", "[single-file]" ) {
    REQUIRE( Factorial(0) == 1 );
}

TEST_CASE( "Factorials of 1 and higher are computed (pass)", "[single-file]" ) {
    REQUIRE( Factorial(1) == 1 );
    REQUIRE( Factorial(2) == 2 );
    REQUIRE( Factorial(3) == 6 );
    REQUIRE( Factorial(10) == 3628800 );
}

TEST_CASE("Date-time string is too short", "[]")
{
    tm_bcd tm;
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
    REQUIRE(parse_iso_date_time(&tm, "2019-03-23 09:32:50") == 1);
}




int main()
{
    char VALID_TIME[] = "2019-03-23 09:32:50";
    char INVALID_TIME[] = "2019-02-11 09:32_40";
    struct tm_bcd rtc_frame;
    char res = parse_iso_date_time(&rtc_frame, INVALID_TIME);
    if (res == 0) {
        printf("%02X-%02X-%02X %02X:%02X:%02X\n",
                rtc_frame.tm_bcd_year,
                rtc_frame.tm_bcd_mon,
                rtc_frame.tm_bcd_mday,
                rtc_frame.tm_bcd_hour,
                rtc_frame.tm_bcd_min,
                rtc_frame.tm_bcd_sec
              );
    }
    else {
        puts(INVALID_TIME);
        printf("%*c\n",res + 1, '^');
    }
    return 0;
}

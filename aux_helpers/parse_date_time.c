#include <stdio.h>
#include <ctype.h>

struct tm_bcd {
    // date
    unsigned char tm_bcd_sec; // Seconds [0, 60]
    unsigned char tm_bcd_min; // Minutes [0, 59]
    unsigned char tm_bcd_hour; // Hour [0, 23]
    
    unsigned char tm_bcd_mday; // Day of week [0, 6]
    unsigned char tm_bcd_mon;  // Month of year [0, 11]
    unsigned char tm_bcd_year; // Years since 1900 mod 100

    char is_year_20XX:1;     
} tm_bcd;

#define PARSE_ERROR 0xFF

/* Returns two BCD digits encoded in char or 0xFF if error occured */
static unsigned char two_chars_to_bcd8u(const char* s)
{
    if (isdigit(s[0]) && isdigit(s[1])) 
        return ((s[0] - '0') << 4) | (s[1] - '0');
    else
        return PARSE_ERRROR;
}

char parse_iso_date_time(struct tm_bcd* f, const char* s)
{
    const char DATE_MEMBER_SEPARATOR = '-';
    const char TIME_MEMBER_SEPARATOR = ':';

    const char* p = s;
    while (isspace(*p)) 
        p++;

    if (p[0] == '1' && p[1] == '9')
        f->is_year_20XX == 0;
    else
        if (p[0] == '2' && p[1] == '0')
            f->is_year_20XX = 1;
        else
            return p - s;
    
    p += 2;

    const unsigned char PARSE_ERROR = 0xFF;

    // parse date

    if ( ( f->tm_bcd_year = two_chars_to_bcd8u(p) ) == PARSE_ERROR )
        return p - s;
    p+=2;

    if (*p == DATE_MEMBER_SEPARATOR)
        p++;
    else
        return p - s;

    if ( ( f->tm_bcd_mon = two_chars_to_bcd8u(p) ) == PARSE_ERROR )
        return p - s;
    p+=2;
    
    if (*p == DATE_MEMBER_SEPARATOR)
        p++;
    else
        return p - s;

    if ( ( f->tm_bcd_mday = two_chars_to_bcd8u(p) ) == PARSE_ERROR )
        return p - s;
    p+=2;

    // ISO format says that time is separated by 'T', we allow a space too.
    if (*p == 'T' || isspace(*p)) 
       p++;
    else
       return p - s; 

    // parse time 

    if ( ( f->tm_bcd_hour = two_chars_to_bcd8u(p) ) == PARSE_ERROR )
        return p - s;
    p+=2;

    if (*p == TIME_MEMBER_SEPARATOR)
        p++;
    else
        return p - s;

    if ( ( f->tm_bcd_min = two_chars_to_bcd8u(p) ) == PARSE_ERROR )
        return p - s;
    p+=2;
    
    if (*p == TIME_MEMBER_SEPARATOR)
        p++;
    else
        return p - s;

    if ( ( f->tm_bcd_sec = two_chars_to_bcd8u(p) ) == PARSE_ERROR )
        return p - s;

    return 0;
}



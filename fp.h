#ifndef __FP_H__
#define __FP_H__

/* Simple fixed point number library
 * Author: P. Bialkowski (hkt)
*/

/* Adjust the two defines below to modify precision */

#define FP_FRAC_BITS 4
#define FP_INTEGER_BITS 8


#define FP_SCALE (1<<FP_FRAC_BITS)
#define FP_EPS (1.0f/FP_SCALE)
#define FP_TOTAL_BITS (FP_FRAC_BITS + FP_INTEGER_BITS)

typedef struct {
    int _v:FP_TOTAL_BITS;
} fp_t;

/* Makes an fixed point number out of standard numeric type */
#define fp(x) (fp_t){(int)((x>0) ? ((x)*FP_SCALE + 0.5):((x)*FP_SCALE - 0.5)) }

#define fpf(x) (((float) x._v)/FP_SCALE )

#define fptrunc(x) ((x._v > 0)?((int)x._v >> FP_FRAC_BITS) : (((int)x._v >> FP_FRAC_BITS) + 1))

#define fpfract(x) ((x._v) - fptrunc(x)*FP_SCALE)

static void fpassign_MSByte_first(fp_t* fpnum, const unsigned char* s) {
    int i;
    fpnum->_v = 0;
    const unsigned char FP_SIZE = (FP_TOTAL_BITS + 7)/8;
    for (i = 0; i< FP_SIZE; ++i) {
        fpnum->_v <<= 8;
        fpnum->_v += (int)s[i];
    }
}

static void fpassign_LSByte_first(fp_t* fpnum, const unsigned char* s) {
    int i;
    fpnum->_v = 0;
    const unsigned char FP_SIZE = (FP_TOTAL_BITS + 7)/8;
    for (i = 0; i< FP_SIZE; ++i) {
        fpnum->_v <<= 8;
        fpnum->_v += (int)s[FP_SIZE - i - 1];
    }
}


static char* utoa10(int val, char* s)
{
    if (val > 10)
        s = utoa10(val / 10, s);
    *s++ = (val % 10) + '0'; 
    return s;
}

static char* fptoa(fp_t x, char* s)
{
    char *p = s;
    if (x._v < 0) {
         *p++='-';
         x._v = -x._v;
      }
    int intpart = fptrunc(x);
    p = utoa10(intpart, p);

    int fracpart = fpfract(x);
    if (fracpart) {
        *p++ = '.';
        while (fracpart > 0) {
            fracpart*=10;
            *p++ = (fracpart/FP_SCALE) + '0';
            fracpart %= FP_SCALE;
        }
    }
    *p = 0;
    return s;
}







#endif /* __FP_H__ */

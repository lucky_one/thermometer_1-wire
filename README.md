Termometr 1-wire z zapisem temperatury
=======================
Funkcje
------------
+ Konieczne
    1. Odczyt temperatury za pomocą skalibrowanego u.s. **DS18B20**.
    2. Wyświetlanie temperatury na zintegrowanym wyświetlaczu.
    3. Zapis temperatury z czasami pomiaru.
    4. Przesłanie zapisów o temperaturze do komputera.
+ Niekonieczne
    1. Wyświetlanie wcześniejszych pomiarów na wyświetlaczu.
    2. Obsługa wielu czujników.
 
Budowa
------------
#### MCU i przyporządkowanie wyprowadzeń
Jako jednotka obliczeniowa pracuje **ATtiny4313** z zegarem
wewnętrznym RC. Docelowo z oscylatorem zewnętrznym 4MHz.

```
             +----u----+ 
    RST PA2 -| 1    20 |- VCC
    RXD PD0 -|         |- PB7 SDA 
    TXD PD1 -|         |- PB6 SCL
    XT1 PA1 -|         |- PB5  
    XT2 PA0 -|         |- PB4 LCD_RST
    BTN PD2 -|         |- PB3 LCD_CS
        PD3 -|         |- PB2 LCD_DC
        PD4 -|         |- PB1 LCD_SPI_MOSI 
    1WR PD5 -|         |- PB0 LCD_SPI_CLK
    GND     -| 10   11 |- PD6 -
             +---------+     
```

Funkcje
------------
### Obsługa zaniku zasilania.
Można wykorzystać komparator analogowy (wejście AIN1) podłączając
wejście AIN0 do wewnętrznego wzorca.

Test można wykonać każąc zapisywać ramki kolejno inkrementowane co
sekundę i sprawdzić potem na zrzucie pamięci czy pojawiły się
nieprawidłowe ramki.

#### Przykład dla ramki 16-bajtowej. 

| Adres (od-do) | Rozmiar [b] | Znaczenie |
|---------------|:------------|:----------|
| 0x00 - 0x07   | 64          | Czas      |
| 0x08 - 0x10   | 12          | RH        |
| 0x10 - 0x12   | 12          | Temp.     |
| 0x13 - 0x15   | 16          | Jasność   |

1. Zapisujemy wszystkie bajty przyszłej ramki bajtami `0xff`.
2. W przypadku przerwania zasilania znajdujemy najdłuższy ciąg bajtów `0xff`.

Obsługa
------------

#### Interface RS-232
Interface wysyła co interwał parametry, które sam wewnętrznie loguje.
Funkcja ta działa do momentu wejścia w tryb przesyłania danych.

W trybie przesyłania danych wysyłane są wszystkie zapisane dane
w postaci:
**TIMESTAMP\tT(internal)\t T(external)\t RELHUM\t LUMINOCITY\r\n**

#### Wyświetlacz
Wyświetlacz pochodzi z telefonu `SIEMENS C55`. W sterowaniu jest zgodny z u.s.
**PCF8812**. Jest oparty na interfejsie szeregowym SPI. Maksymalne taktowanie
linii SCL to 4MHz. Ma wewnętrzny generator podwyższonego napięcia (max 9V) do
zasilania segmentów wyświetlacza. Przydatne, gdy wyśiwetlacz jest zasilany
napięciem 3,3V. W tym projekcie zastosowane ogólne zasilanie o nap.  5V wymaga
wyłączenia generatora przy inicjalizacji wyświetlacza.

Patrząc na wyświetlacz od strony wyprowadzeń, pinami do góry,
od lewej strony widzimy piny o funkcjach:

   [0] VDD, [1] CLK, [2] DAT, [3] DC, [4] CS, [5] GND, [6] HVdd, [7] RST

Konieczne jest wstawienie kondensatora 1-2uF pomiędzy HVdd a GND.

#### Znaki pisma

```
0 1 2 3 4 5 6 7 8 9 C T R H L
stopień
: %
temperatura
upload
komputer
transfer
zegarek 
powrót
```

#### Przyciski sterujące

Będą potrzebne trzy przyciski [UP], [DOWN] oraz [ZATWIERDŹ/MENU].
Ponieważ procesor ma tylko 2 bezpośrednie przerwania lepiej będzie skanować
przyciski.




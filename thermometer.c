#include <util/delay.h>
#include "thermometer.h"
#include "onewire.h"
#include "fp.h"

void thermometer_init()
{
    onewire_init();
}

char* read_thermometer_id(char* d)
{
    if (!d)
        return 0;
    if (!onewire_reset_probe())
        return 0;
    onewire_write(ONEWIRE_READ_ROM);
    int i;
    for (i = 0; i < THERMOMETER_ID_SIZE; ++i) {
        d[i] = onewire_read();
    }
    return d;
}

fp_t read_temperature(void)
{
    const unsigned char SCRATCHPAD_SIZE = 9;
    uint8_t scratchpad[SCRATCHPAD_SIZE];
    onewire_reset_probe();
    onewire_write(ONEWIRE_SKIP_ROM);
    onewire_write(DS18B20_CONVERT_T);
    _delay_ms(750);
    onewire_reset_probe();
    onewire_write(ONEWIRE_SKIP_ROM);
    onewire_write(DS18B20_READ_SCRATCHPAD);
    int i;
    for (i = 0; i<SCRATCHPAD_SIZE; ++i) {
        scratchpad[i] = onewire_read();
    }
    fp_t temperature;
    fpassign_LSByte_first(&temperature, scratchpad);
    return temperature;
}

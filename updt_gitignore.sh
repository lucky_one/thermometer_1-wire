#!/bin/bash

T=$(mktemp)
# Add all executable files without extension
(cat .gitignore; find . -executable -type f ! -name '*.*' | sed -e 's%^\./%%') | sort | uniq >$T;
mv $T .gitignore

echo "AFTER UPDATE: "
echo
more .gitignore

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include "uart.h"
#include <avr/interrupt.h>
#include <stdbool.h>
#include <inttypes.h>
#include <assert.h>
#include "onewire.h"
#include "thermometer.h"
#include "fp.h"
#include "lcd_c55.h"
#include "font.h"
#include "utf8.h"
#include "sft_i2c.h"

#define RINGBUF_SIZE 4
#define RINGBUF_ITEM_T fp_t
#include "ringbuf.h"

RINGBUF g_ringbuf;
FNFont g_font;

//void timer1_init(void);

/*
ISR (TIMER1_COMPA_vect, ISR_NAKED)
{
    system_tick();
    reti();
}
*/

volatile uint8_t vop = 0x80;
volatile char vop_to_change;

ISR(INT0_vect)
{
    vop_to_change = 1;
    _delay_ms(10); // debouncing
    uart_puts_P(PSTR("vop: ")); uart_puthex8(vop);
    vop++;
    if (vop == 0xFF)
        vop = 0x80;
}

/*
void timer1_init()
{
    // TODO prescaler should be estimated in compilation time
    // as a function of F_CPU
    // set up timer with prescaler = 64 and CTC mode
    TCCR1B |= (1 << WGM12)|(1 << CS11)|(1 << CS10);
    // initialize counter
    TCNT1 = 0;
    // initialize compare value
    _Static_assert(((long)F_CPU)/64 < 0xFFFF,
            "F_CPU/64 exceeds Timer1 countability\n");
    OCR1A = F_CPU/64 - 1;

    // enable compare interrupt
    TIMSK |= (1 << OCIE1A);
}
*/

uint8_t read_font_data(const char* p)
{
    return eeprom_read_byte((uint8_t*)p);
} 

void font_init(FNFont* font)
{
    font->read_data=read_font_data;
    bool config_ok = fn_config(font, FN_VDIR_BOTTOM_UP | FN_LEFT_TO_RIGHT |
                                     FN_SCAN_VERTICAL | FN_ALIGNED);

    if (config_ok)
        uart_puts("Fn OK");
    else {
        uart_puts("Fn error: ");
        uart_puthex8(font->error_code);
        uart_puts("");
    }
}

void init(void)
{
//   timer1_init();
//    rb_reset(&g_ringbuf);
    i2c_init(); 
    uart_init();
//    lcd_init();
    //font_init(&g_font);
}

void lcd_write(const char* s)
{
    char chrlen;
    while ( (chrlen = utf8chrlen(s)) ) {
        char y; 
        for (y = 0; y < g_font.width; ++y) {
            lcd_send(DATA, fn_byte(&g_font, s, y));
        }
        while (y++ < g_font.xadvance)
            lcd_send(DATA, 0);
        s += chrlen;
    }
} 

void lcd_settings(void)
{
    if (vop_to_change) {
            lcd_send(COMMAND, 0x21); // extended command set
            lcd_send(COMMAND, vop);
            lcd_send(COMMAND, 0x20);
            uart_puts("vch");
            uart_puthex8(vop); uart_putchar('\n');
            vop_to_change = 0;
    }
}    

/*
uint8_t read_mem(uint16_t addr)
{
    i2c_start(0xA0 | I2C_READ);
    i2c_write(addr >> 8);
    i2c_write(addr & 0xFF);
    uint8_t data = i2c_read(true);
    i2c_stop();
    return data;
}

void write_mem(uint16_t addr, uint8_t data)
{
    i2c_start(0xA0 | I2C_WRITE);
    i2c_write(addr >> 8);
    i2c_write(addr & 0xFF);
    i2c_write(data); i2c_stop();
}
*/


int main(void)
{
    init();
    const uint8_t DEV24C02 = 0xA0;
    unsigned char result, i = 0xBB;
    for(;;) {
        _delay_ms(500);
        uart_puts("start");
        if (!i2c_start(DEV24C02 + I2C_WRITE))    // set device
            uart_puts("> FAILED: start + ACK");
        if (!i2c_write(0x02))
            uart_puts("> FAILED: write + ACK");
        if (!i2c_write(0x34))
            uart_puts("> FAILED: write + ACK");
        if (!i2c_write(i))
            uart_puts("> FAILED: write + ACK");
        i2c_stop();   
        _delay_ms(20);
        if (!i2c_start(DEV24C02 + I2C_WRITE))    // set device
            uart_puts("> FAILED: start + ACK");
        if (!i2c_write(0x02))
            uart_puts("> FAILED: write + ACK");
        if (!i2c_write(0x34))
            uart_puts("> FAILED: write + ACK");
        if (!i2c_rep_start(DEV24C02 + I2C_READ))
            uart_puts("> FAILED: rep start");
        result = i2c_read(0);
        i2c_stop();
        if (result == i)
            uart_puts("Value is correct");
        else
            uart_puts("Value incorrect");
        uart_puthex8(result);
        uart_puts("");
    }
}

#ifndef __SFT_SPI__
#define __SFT_SPI__
#include <util/delay.h>
#include <stdint.h>

#define SPI_DELAY_US 16

#ifndef SPI_PORT
#define SPI_PORT PORTB
#endif

#ifndef SPI_DIRP
#define SPI_DIRP DDRB
#endif

#ifndef SPI_CLK_PIN
#define SPI_CLK_PIN 0
#endif

#ifndef SPI_MOSI_PIN
#define SPI_MOSI_PIN 1
#endif

static void spi_init()
{
     SPI_PORT &= ~((1 << SPI_MOSI_PIN) | (1 << SPI_CLK_PIN));
     SPI_DIRP |= (1 << SPI_MOSI_PIN) | (1 << SPI_CLK_PIN);
}

static void spi_send(uint8_t byte)
{
    char bitno = 7;
    do {
        SPI_PORT &= ~(1 << SPI_MOSI_PIN);
        if (byte & 0x80)
            SPI_PORT |= (1 << SPI_MOSI_PIN);
        _delay_us(SPI_DELAY_US);
        SPI_PORT |= (1 << SPI_CLK_PIN); // clk hi
        byte <<= 1;
        _delay_us(SPI_DELAY_US);
        SPI_PORT &= ~(1 << SPI_CLK_PIN); // clk lo
    } while (bitno--);
    SPI_PORT &= ~(1 << SPI_MOSI_PIN); // pull down at the end
}

#endif /* __SFT_SPI_  */

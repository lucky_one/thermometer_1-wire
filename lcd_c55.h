#ifndef __LCD_C55__
#define __LCD_C55__
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

/* Pin definitions */

#define LCD_RST_DIRP DDRB
#define LCD_RST_PORT PORTB
#define LCD_RST_PIN 4

#define LCD_CS_DIRP DDRB
#define LCD_CS_PORT PORTB
#define LCD_CS_PIN 3

#define LCD_DC_DIRP DDRB
#define LCD_DC_PORT PORTB
#define LCD_DC_PIN 2

#define SPI_DIRP DDRB
#define SPI_PORT PORTB
#define SPI_CLK_PIN 0
#define SPI_MOSI_PIN 1

/* --------------- */

#include "sft_spi.h"

#define LCD_BANKS 8
#define LCD_PIXEL_COLUMNS 102

typedef enum { DATA, COMMAND } DC_T;

void lcd_init(void);
void lcd_send(DC_T, uint8_t);
void lcd_move(int x, int y);
void lcd_clear(void);


#endif /* __LCD_C55__ */

#ifndef __FONT_H__
#define __FONT_H__
#include <stdbool.h>
#include <stdint.h>

typedef struct
{
    union {
    uint32_t glyph_data_start;
    char error_code;
    };
    uint16_t width;
    uint16_t height;
    uint16_t xadvance;
    uint16_t yadvance;
    uint8_t (*read_data)(const char*);
} FNFont;

typedef enum { FN_VDIR, FN_HDIR, FN_SCAN, FN_ALIGNMENT } FN_BITPATH_PARAM;

enum {
    FN_VDIR_BOTTOM_UP = 1<<FN_VDIR,
    FN_LEFT_TO_RIGHT = 1<<FN_HDIR,
    FN_SCAN_VERTICAL = 1<<FN_SCAN,
    FN_ALIGNED = 1<<FN_ALIGNMENT
};

#define FN_ACCESS_FUNCTION_ERROR 1 
#define FN_SIGNATURE_ERROR 2
#define FN_UNSUPPORTED_VERSION 3
#define FN_BITPATH_MISMATCH 4
#define FN_UNSUPPORTED_METRICS 5

/* Configure font with a defined bitpath.
 * Return true if succeeded.
*/
bool fn_config(FNFont*, uint8_t device_bitpath);

/* Find offset of a character.
 * Retunrn FN_NPOS if not found.
*/
int fn_find(const FNFont*, const char* uch);

/* Get byte of a glyph image */
uint8_t fn_byte(const FNFont*, const char* uch, uint8_t byteno);



#endif /* __FONT_H__ */

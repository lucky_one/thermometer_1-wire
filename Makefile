MCU:=attiny4313
DUDE_MCU=t4313
DEFS=-D F_CPU=1000000UL
CC=avr-gcc
OBJPROG=avr-objcopy
CFLAGS=-s -Os -DNDEBUG -Wno-unused-function\
	   -Wall -pedantic \
	   -mcall-prologues \
	   -fpack-struct \
	   -ffunction-sections \
	   -fdata-sections \
	   -fno-strict-aliasing \
	   -fshort-enums \
	   -flto
LDOPTS=-flto -fuse-linker-plugin -Wl,-gc-sections -fno-strict-aliasing
#LIBS=-Wl,-u,vprintf -lprintf_min -lm
LDOPTS=-flto -fuse-linker-plugin
LIBS=-lprintf_flt -lm
AVRDUDE_BAUDRATE=57600
TTY=/dev/ttyUSB0

all: program.hex font printsize clean

# START SOURCE COMPILE UNITS SECTION

program.hex: program.elf
	$(OBJPROG) -j .text -j .data -O ihex $^ $@

program.elf: main.o onewire.o uart.o thermometer.o lcd_c55.o font.o sft_i2c.o
	$(CC) $^ -mmcu=$(MCU) $(LIBS) $(LDOPTS) -o $@

main.o: main.c onewire.h fp.h ringbuf.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

thermometer.o: thermometer.c thermometer.h onewire.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

onewire.o: onewire.c onewire.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

uart.o: uart.c uart.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

lcd_c55.o: lcd_c55.c lcd_c55.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

font.o: font.c font.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@

sft_i2c.o: sft_i2c.c sft_i2c.h
	$(CC) -mmcu=$(MCU) $(CFLAGS) $(DEFS) $< -c -o $@


# END SOURCE COMPILE UNITS SECTION
#

# START FONT COMPILE UNITS SECTION
FONTC=fontc/fontc

$(FONTC): fontc/Makefile
	make -C fontc

font.bin: font.txt $(FONTC)
	$(FONTC) -o $@ $<

font.eep: font.bin
	avr-objcopy -I binary -O ihex $< $@

font: font.bin font.eep
	@echo "Font size: " `wc -c <$<` B

# END FONT COMPILE UNITS SECTION
#

burnusbasp: program.hex
	avrdude -p $(DUDE_MCU) -c usbasp -P usb -u -U flash:w:program.hex

burnponyser: program.hex
	avrdude -p $(DUDE_MCU) -c ponyser -P $(TTY) -u -U flash:w:program.hex \
		-b $(AVRDUDE_BAUDRATE)

burnbuspirate: program.hex
	avrdude -p $(DUDE_MCU) -c buspirate -P /dev/buspirate -u -U \
	   	flash:w:program.hex

burnfontponyser: font.eep
	avrdude -p $(DUDE_MCU) -c ponyser -P $(TTY) -u -U  hfuse:w:0xd1:m
	avrdude -p $(DUDE_MCU) -c ponyser -P $(TTY) -u -U eeprom:w:font.eep \
		-b $(AVRDUDE_BAUDRATE)
	avrdude -p $(DUDE_MCU) -c ponyser -P $(TTY) -u -U  hfuse:w:0x91:m

printsize: program.elf
	avr-size -C --mcu=$(MCU) $^

clean:
	rm -f *.o *.elf *.bin

#!/bin/bash

echo "This file tests values for UART which are calculated"
echo "by avr-libc macro in util/setbaud.h ."

NAME=uartvals

TNAME="$NAME"_t
INC=/usr/avr/include/

cat > $TNAME.c << EOCFILE
#include <stdio.h>
#define F_CPU 4000000
#define BAUD 9600
#include <util/setbaud.h>
int main() {
#if USE_2X
printf("USE_2X:1 UBRRL_VALUE: %d UBRRH_VALUE: %d\n", UBRRL_VALUE, UBRRH_VALUE);
#else
printf("USE_2X:0, UBRRL_VALUE: %d, UBRRH_VALUE: %d\n", UBRRL_VALUE,UBRRH_VALUE);
#endif
return 0;
}
EOCFILE

echo "Compiling test"
gcc -s -o $TNAME $TNAME.c -I$INC
echo "Running test"
./$TNAME
echo "Cleaning"
rm ./$TNAME $TNAME.c

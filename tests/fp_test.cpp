#include <stdio.h>
#include "../fp.h"

#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_FAST_COMPILE               // Sacrifices some (rather minor) features for compilation speed
#define CATCH_CONFIG_DISABLE_MATCHERS           // Do not compile Matchers in this compilation unit
#include "catch.hpp"

class fp_test_values {
    public:
    static constexpr const int COUNT = 8;
    fp_t values[COUNT];
    float fvalues[COUNT];

    fp_test_values()
    {
        // A few examples from DS18B20 documentation
        // The chip sends bytes in reverse order pay attention
        unsigned char t125[] = { 0x07, 0xd0 };
        unsigned char tminushalf[] = { 0xff, 0xF8 };
        unsigned char tplushalf[] = { 0x00, 0x08 };
        unsigned char t250625[] = { 0x01, 0x91 };
        unsigned char t85[] = { 0x05, 0x50 };
        unsigned char reversed_t85[] = { 0x50, 0x05};
        // My test cases
        fp_t minusonehalf = fp(-1.5);
        fp_t minusalmosthalf = fp(-0.425);
        
        
        fpassign_MSByte_first(&values[0], t125);
        fvalues[0] = 125.0f;

        fpassign_MSByte_first(&values[1], tminushalf);
        fvalues[1] = -0.5f;

        fpassign_MSByte_first(&values[2], tplushalf);
        fvalues[2] = +0.5f;

        fpassign_MSByte_first(&values[3], t250625);
        fvalues[3] = +25.0625f;

        fpassign_MSByte_first(&values[4], t85);
        fvalues[4] = 85.0f;

        fpassign_LSByte_first(&values[5], reversed_t85);
        fvalues[5] = fvalues[4]; // Because we assign from LSB 

        values[6] = minusonehalf;
        fvalues[6] = -1.5f;

        values[7] = minusalmosthalf;
        fvalues[7] = -0.425f;
    }
};

constexpr const int fp_test_values::COUNT;

TEST_CASE_METHOD(fp_test_values, "fp_t", "[temperature]")
{
    for (int i = 0; i < COUNT; ++i) {
        float converted = fpf(values[i]);
        REQUIRE(converted == Approx(fvalues[i]).epsilon(FP_EPS));
    }
} 

TEST_CASE_METHOD(fp_test_values, "cstring", "[temperature]")
{
    const char BUF_SIZE = 20;
    char snprintf_s[BUF_SIZE], fptoa_s[BUF_SIZE];
    for (int i = 0; i < COUNT; ++i) {
       snprintf(snprintf_s, BUF_SIZE, "%g", fpf(values[i]) );
       fptoa(values[i], fptoa_s);
       REQUIRE(std::string(snprintf_s) == std::string(fptoa_s));
    }
}

#define RINGBUF_ITEM_T int
#define RINGBUF_SIZE 8 
#include "../ringbuf.h"

#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_FAST_COMPILE               // Sacrifices some (rather minor) features for compilation speed
#define CATCH_CONFIG_DISABLE_MATCHERS           // Do not compile Matchers in this compilation unit
#include "catch.hpp"

TEST_CASE("RINGBUF", "[logging]") {
    RINGBUF rb;
    rb_reset(&rb);

SECTION("rb_fill") {
    rb_fill(&rb, 34);
    REQUIRE(rb_get(&rb, 0) == 34);
    REQUIRE(rb_get(&rb, 1) == 34);
    REQUIRE(rb_get(&rb, 2) == 34);
    REQUIRE(rb_get(&rb, 3) == 34);
    REQUIRE(rb_get(&rb, 4) == 34);
    REQUIRE(rb_get(&rb, 5) == 34);
    REQUIRE(rb_get(&rb, 6) == 34);
    REQUIRE(rb_get(&rb, 7) == 34);
}

SECTION("rb_push_front") {
    int values[8] = {1,1,2,3,5,8,13,21 };
    for (int i = 0; i < sizeof(values)/sizeof(values[0]); ++i) {
        rb_push_front(&rb, values[i]);
    }

    for (int i = 0; i < sizeof(values)/sizeof(values[0]); ++i) {
        int retrieve = rb_get(&rb, i);
        REQUIRE(retrieve == values[i]);
    }
}

SECTION("rb_push_front - overflow") {
    int values[12] = {1,1,2,3,5,8,13,21,34,55,89,144 };
    for (int i = 0; i < sizeof(values)/sizeof(values[0]); ++i) {
        rb_push_front(&rb, values[i]);
    }
    for (int i = 0; i < RINGBUF_SIZE; ++i) {
        int retrieve = rb_get(&rb, i);
        const int VALUES_SIZE = sizeof(values)/sizeof(values[0]);
        const int pushed_out_from_buffer = VALUES_SIZE - RINGBUF_SIZE;
        int idx = i + pushed_out_from_buffer;
        REQUIRE(retrieve == values[idx]);
    }
}

}

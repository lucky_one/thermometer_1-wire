#ifndef __THERMOMETER_H__
#define __THERMOMETER_H__
#include "fp.h"

#define THERMOMETER_ID_SIZE 8

void thermometer_init();
char* read_thermometer_id(char*);
fp_t read_temperature(void);



#endif /* __THERMOMETER_H__ */
